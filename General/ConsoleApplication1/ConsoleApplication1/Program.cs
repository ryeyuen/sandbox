﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClosedXML.Excel;
using NSubstitute;


namespace ConsoleApplication1
{
    public class Program
    {
        static void Main(string[] args)
        {
            //var workBook = new XLWorkbook(@"C:\Projects\zSandbox\ClosedXML\ClosedXML\ConsoleApplication1\CreditRisk.xlsm");

            var car = Substitute.For<ICar>();
            car.engine = "Hello";
            car.tires = "Black";

            var x = new CarDealer(car);



        }



        public interface ICar
        {
            string engine { get; set; }
            string tires { get; set; } 
        }

        public class Honda : ICar
        {
            public string engine { get; set; }
            public string tires { get; set; }
        }

        public class CarDealer
        {
            public ICar Car { get; set; }

            public CarDealer(ICar car)
            {
                Car = car;
            }
        }




    }
}
