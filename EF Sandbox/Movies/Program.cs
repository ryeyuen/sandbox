﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Movies
{
    
    // MAIN POINTS TO CONSIDER
    // 1. When attaching One-To-One Children to Concrete Classes derived from an abstract, Eg. (Ride deriving from abstract Car), you can Foreign Key 
    //    the Child directly to the Concrete even though the Concrete doesn't have its own key. Entity framework is smart enough to deduce the key lies in the 
    //    abstract and not the concrete. See the Spoiler to Ride implementation below for a working example of how to do this. 

    //    One important note. Ensure that the Concrete class doesn't have a key (Eg. Ride Class would normally have RideId). Since RideId isn't actually a 
    //    functional key, because the true key is the abstract CarId, the Child will incorrectly link to the non-functioning RideId. So, the concrete
    //    class must have no ID property, execpt for the ID inherited from the abstract. See the Ride Class for an example of this.

    // 2. When retrieving an abstract class from the context, and wanting to include related properties (with lazy loading off), use the following code snippet to cast and then include
    //     var test = context.Car.OfType<Ride>().Include(b => b.Spoiler).ToList();                    

    // 3. When loading an object from context, and lazy loading is off, all related entities need to be loaded manually.
    
    
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<TestContext>());
            using (var context = new TestContext())
            {

                // Setup Db Values
                var ride1 = new Ride() { Name = "Gomer" };
                var spoiler1 = new Spoiler { Colour = "Black" };

                ride1.Spoiler = spoiler1;

                context.Car.Add(ride1);
                context.SaveChanges();

                var ride2 = new Ride() { Name = "Crasher" };
                var spoiler2 = new Spoiler { Colour = "Red" };

                ride2.Spoiler = spoiler2;

                context.Car.Add(ride2);
                context.SaveChanges();



                //// View Db Values
                //var test = context.Car.OfType<Ride>().Include(b => b.Spoiler).ToList();                

                //var bbb = context.Car.OfType<Ride>().First().Spoiler;
               
            }


        }
    }

    public abstract class Car
    {
        public int CarId { get; set; }    
    }


    public class Ride : Car
    {
        public Spoiler Spoiler { get; set; }
        public string Name { get; set; }

    }


    public class Spoiler
    {
        //[Key, ForeignKey("Ride")]
        public int RideId { get; set; }
        public Ride Ride { get; set; }
        public string Colour { get; set; }
    }

    public abstract class Movie
    {
        public int TheaterId { get; set; }
        public virtual Theater Theater { get; set; }

        public string Title { get; set; }
        public string Genre { get; set; }
        public decimal Price { get; set; }

    }

    public class ActionMovie : Movie
    {
        public int BodyCount { get; set; }
    }

    public class Theater
    {
        public int TheaterId { get; set; }
        public virtual Movie Movie { get; set; }
        public string Name { get; set; }
    }


    public interface IFrameStorage
    {
        string foo { get; set; }
    }

    public class TableFrameStorage : IFrameStorage
    {
        public int BId { get; set; }
        public string foo { get; set; }
        public int MyProperty { get; set; }
    }

    public abstract class FrameDefinition 
    {
        public int MyProperty2 { get; set; }

        public abstract IFrameStorage Run();
    }

    public class FrameDefinitionTable : FrameDefinition 
    {
        public override IFrameStorage Run()
        {
            return new TableFrameStorage();
        }
    }
    


    public class TestContext : DbContext
    {
     //   public DbSet<Movie> Movies { get; set; }
      //  public DbSet<Theater> Theaters { get; set; }
        //public DbSet<Car> Car { get; set; }
        public DbSet<Spoiler> Spoiler { get; set; }
        public DbSet<Car> Car { get; set; }

        //public DbSet<B> Concrete { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Spoiler>().HasKey(t => t.RideId);

            modelBuilder.Entity<Ride>().HasOptional(t => t.Spoiler).WithRequired(t => t.Ride);



            //var serv = PluralizationService.CreateService(new System.Globalization.CultureInfo("en-us"));

            //modelBuilder.Types()
            // .Configure(entity => entity.ToTable("MyPrefix_" + entity.ClrType.Name));

            

            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //modelBuilder.HasDefaultSchema("MySchema"); // Replace MySchema with the name of your prefix

            //modelBuilder.Entity<Movie>()
            //.HasKey(t => t.TheaterId);

            //modelBuilder.Entity<Theater>()
            //.HasRequired(t => t.Movie)
            //.WithRequiredPrincipal(t => t.Theater);

            //        modelBuilder 
            //.Entity<Theater>() 
            //.Property(t => t.Movie) 
            //.HasColumnAnnotation( 
            //    "Index",  
            //    new IndexAnnotation(new[] 
            //        { 
            //            new IndexAttribute("Index1"), 
            //            new IndexAttribute("Index2") { IsUnique = true } 
            //        })));

        }
    }    
}
