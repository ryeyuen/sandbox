﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Drawing.Printing;
using System.Text.RegularExpressions;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        int i = 0;
        PrintAction printAction = PrintAction.PrintToFile;

        public Form1()
        {
            InitializeComponent();            
            printDocument1.OriginAtMargins = true;
            printPreviewDialog1.Document = printDocument1;
        }        

        private void printDocument1_PrintPage(System.Object
            sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            //e.Graphics.DrawImage(memoryImage, 0, 0);


            //http://stackoverflow.com/questions/8761633/how-to-find-the-actual-printable-area-printdocument

            Graphics g = e.Graphics;

            // If you set printDocumet.OriginAtMargins to 'false' this event 
            // will print the largest rectangle your printer is physically 
            // capable of. This is often 1/8" - 1/4" from each page edge.
            // ----------
            // If you set printDocument.OriginAtMargins to 'false' this event
            // will print the largest rectangle permitted by the currently 
            // configured page margins. By default the page margins are 
            // usually 1" from each page edge but can be configured by the end
            // user or overridden in your code.
            // (ex: printDocument.DefaultPageSettings.Margins)

            // Grab a copy of our "soft margins" (configured printer settings)
            // Defaults to 1 inch margins, but could be configured otherwise by 
            // the end user. You can also specify some default page margins in 
            // your printDocument.DefaultPageSetting properties.
            RectangleF marginBounds = e.MarginBounds;

            // Grab a copy of our "hard margins" (printer's capabilities) 
            // This varies between printer models. Software printers like 
            // CutePDF will have no "physical limitations" and so will return 
            // the full page size 850,1100 for a letter page size.
            RectangleF printableArea = e.PageSettings.PrintableArea;

            // If we are print to a print preview control, the origin won't have 
            // been automatically adjusted for the printer's physical limitations. 
            // So let's adjust the origin for preview to reflect the printer's 
            // hard margins.
            if (printAction == PrintAction.PrintToPreview)
                g.TranslateTransform(printableArea.X, printableArea.Y);

            // Are we using soft margins or hard margins? Lets grab the correct 
            // width/height from either the soft/hard margin rectangles. The 
            // hard margins are usually a little wider than the soft margins.
            // ----------
            // Note: Margins are automatically applied to the rotated page size 
            // when the page is set to landscape, but physical hard margins are 
            // not (the printer is not physically rotating any mechanics inside, 
            // the paper still travels through the printer the same way. So we 
            // rotate in software for landscape)
            int availableWidth = (int)Math.Floor(printDocument1.OriginAtMargins ? marginBounds.Width : (e.PageSettings.Landscape ? printableArea.Height : printableArea.Width));
            int availableHeight = (int)Math.Floor(printDocument1.OriginAtMargins ? marginBounds.Height : (e.PageSettings.Landscape ? printableArea.Width : printableArea.Height));

            // Draw our rectangle which will either be the soft margin rectangle 
            // or the hard margin (printer capabilities) rectangle.
            // ----------
            // Note: we adjust the width and height minus one as it is a zero, 
            // zero based co-ordinates system. This will put the rectangle just 
            // inside the available width and height.
            g.DrawRectangle(Pens.Red, 0, 0, availableWidth - 1, availableHeight - 1);

            //******************* BEGIN SETUP TEXT MEASUREMENT ************** 
            // Put graphics object into easier to access variable 
            var graphics = e.Graphics;

            // Setup Font 
            var font = new Font("Times New Roman", 18, FontStyle.Regular); // Use bitwise operator "|" to get Bold and Italics simultaneously                

            // Setup graphics rendering options so text measurement is as precise as possible
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            var format = StringFormat.GenericTypographic;

            var text = "Hello";
                      
            if (i < 5)
            {
                e.HasMorePages = true;
                graphics.DrawString(text + i, font, Brushes.Black, 0, 0, format);
                i++;
            }
            else
            {
                e.HasMorePages = false;
            }
        }

        private void printButton_Click(System.Object sender,
            System.EventArgs e)
        {
            //CaptureScreen(); Alternative method of screencapping form and then printing that BMP.
            printPreviewDialog1.Show();
        }

        
        [System.Runtime.InteropServices.DllImport("gdi32.dll")]
        public static extern long BitBlt(IntPtr hdcDest,
            int nXDest, int nYDest, int nWidth, int nHeight,
            IntPtr hdcSrc, int nXSrc, int nYSrc, int dwRop);
        private Bitmap memoryImage;
        private void CaptureScreen()
        {
            Graphics mygraphics = this.CreateGraphics();
            Size s = this.Size;
            memoryImage = new Bitmap(s.Width, s.Height,
                mygraphics);
            Graphics memoryGraphics = Graphics.FromImage(
                memoryImage);
            IntPtr dc1 = mygraphics.GetHdc();
            IntPtr dc2 = memoryGraphics.GetHdc();
            BitBlt(dc2, 0, 0, this.ClientRectangle.Width,
                this.ClientRectangle.Height, dc1, 0, 0,
                13369376);
            mygraphics.ReleaseHdc(dc1);
            memoryGraphics.ReleaseHdc(dc2);
        }

    }
}
