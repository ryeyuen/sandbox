﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Drawing.Printing;
using System.Text.RegularExpressions;


namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
            
            
            
        }

        private void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            // Need to render text for measurement via graphics object. Incorporate into Form's graphics rendering method, to reduce number of rendering passes. 
            CalcHeight(e);
        }

        private void CalcHeight(PaintEventArgs e)
        {
            //******************* BEGIN SETUP TEXT MEASUREMENT ************** 
            // Put graphics object into easier to access variable 
            var graphics = e.Graphics;

            // Setup Font 
            var font = new Font("Times New Roman", 18, FontStyle.Regular); // Use bitwise operator "|" to get Bold and Italics simultaneously                

            // Setup graphics rendering options so text measurement is as precise as possible
            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            var format = StringFormat.GenericTypographic;

            // Setup text measurement rectangle which needs to contain text to be measured. Set to arbitrarily large size to ensure any string will fit.
            RectangleF rectTextMeasurement = new System.Drawing.RectangleF(0, 0, 1000, 1000);

            // Setup regions object that will contain width/height measurement of measured text.
            // Element 0 of this array always contains the measurement info for the whole string, when used in conjunction with MeasureCharacterRanges(further down)
            Region[] regions = new System.Drawing.Region[1];

            // Setup all padding info
            float topPadding = 10;
            var topPaddingRect = new RectangleF(0, 0, 1000, topPadding);

            float rightPadding = 10;
            var rightPaddingRect = new RectangleF(0, 0, rightPadding, 1000);

            float bottomPadding = 10;
            var bottomPaddingRect = new RectangleF(0, 0, 1000, bottomPadding);

            float leftPadding = 10;
            var leftPaddingRect = new RectangleF(0, 0, leftPadding, 1000);

            // Setup text draw point to begin after top padding
            float y = 0;
            y = topPadding;

            // Setup line spacing
            float lineSpacing = 19.2F;

            // TO AVOID OFF-BY-ONE PIXEL ERRORS, NEED TO IMPLEMENT ROUNDING SYSTEM IN FINAL SYSTEM TO ENSURE PROPER ROUNDING METHODS ARE USED.
            // https://trigger.atlassian.net/wiki/display/RR/Unit+Conversions

            //******************* END SETUP TEXT MEASUREMENT **************

            //******************* EXECUTE TEXT WIDTH MEASUREMENT ON ALL TEXT IN TEXTBOX1 **************

            // Run regex to extract each line in textbox
            // Need to redo regex to pickup all non-linebreak characters (Eg !,@,#,)
            var matches = Regex.Matches(textBox1.Text, @"\w+");

            foreach (Match match in matches)
            {

                // Need to resize text measurement rect every call to ensure text will fit within it
                rectTextMeasurement = new System.Drawing.RectangleF(0, 0, 1000, 1000);

                // Get text
                var text = match.ToString();

                // Range has an internal limit of 32 characters wide
                CharacterRange[] ranges = { new System.Drawing.CharacterRange(0, text.Length) };

                format.SetMeasurableCharacterRanges(ranges);

                regions = graphics.MeasureCharacterRanges(text, font, rectTextMeasurement, format);
                rectTextMeasurement = regions[0].GetBounds(graphics);

                //(DEMO PURPOSES ONLY - DRAW NOT REQUIRED IN FINAL SOLUTION) Draw Left Padding
                // Left Padding                    
                graphics.DrawRectangle(new Pen(Color.Blue, 1F), 0, 10, leftPaddingRect.Width, leftPaddingRect.Height);

                //(DEMO PURPOSES ONLY) Draw Top Padding
                // Top Padding                   
                graphics.DrawRectangle(new Pen(Color.Yellow, 1F), 0, 0, topPaddingRect.Width, topPaddingRect.Height);

                //(DEMO PURPOSES ONLY) Render Text and Draw Width/Height Measurement Boxes                     
                graphics.DrawString(text, font, Brushes.Black, leftPadding, y, format);
                graphics.DrawRectangle(new Pen(Color.Red, 1), leftPadding, y, rectTextMeasurement.Width, rectTextMeasurement.Height);
                y = y + lineSpacing;
            }
            //******************* END TEXT MEASUREMENT **************


            //******************* CALCULATE HEIGHT VALUE **************
            // Gets the co-ordinate (Upper left corner of text box, by default) of the draw point of last text-line "box" (y), subtract line spacing since there's one extra linespacing added at
            // end of for-each loop, add final text box's rect height and bottom padding to get final height.
            // This height is assuming drawing started from y = 0 (which it should always do)

            var height = y - lineSpacing + rectTextMeasurement.Height + bottomPadding;
            label1.Text = "Height is: " + height.ToString() + " px (@96PPI)";

            //(DEMO PURPOSES ONLY) Draw Bottom Padding
            graphics.DrawRectangle(new Pen(Color.Orange, 1F), 10, height - bottomPadding, bottomPaddingRect.Width, bottomPaddingRect.Height);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            printPreviewDialog1.ShowDialog();
        }
    }
}