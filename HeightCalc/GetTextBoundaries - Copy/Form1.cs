﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Text;
using System.Drawing.Printing;
using System.Text.RegularExpressions;


namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();

            listBox1.DataSource = GetInstalledFonts();



            var fam = new FontFamily("Times New Roman");
            var gg = fam.GetLineSpacing(FontStyle.Regular);

            var f = new Font("Times New Roman", 12, FontStyle.Regular);

            using (Graphics g = Graphics.FromHwnd(IntPtr.Zero))
            {
                g.PageUnit = GraphicsUnit.Point;
                var size = g.MeasureString("a\na", f);
                var meas = TextRenderer.MeasureText(textBox1.Text, f);

            }

            var pKind = PaperKind.Letter;
            var p = new PaperSize() { RawKind = 1 };

        }

        //protected override void OnPaint(PaintEventArgs e)
        private void Form1_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
        {
            //DrawList(e);
            CalcHeight(e);



            //return (int)(rect.Right + 1.0f);





            //var font = new Font("Times New Roman", 12, FontStyle.Regular);
            //var text = "Berti";

            //System.Drawing.StringFormat format = new System.Drawing.StringFormat();
            //System.Drawing.RectangleF rect = new System.Drawing.RectangleF(0, 0, 1000, 1000);
            //System.Drawing.CharacterRange[] ranges = { new System.Drawing.CharacterRange(0, text.Length) };
            //System.Drawing.Region[] regions = new System.Drawing.Region[1];

            //format.SetMeasurableCharacterRanges(ranges);
            //regions = e.Graphics.MeasureCharacterRanges(text, font, rect, format);
            //rect = regions[0].GetBounds(e.Graphics);


            //e.Graphics.DrawString(text, font, Brushes.Black, new PointF(10, 10));
            //e.Graphics.DrawRectangle(new Pen(Color.Red, 1), 10, 10, rect.Width, rect.Height); // tt2[0].GetBounds(e.Graphics).Width, tt2[0].GetBounds(e.Graphics).Height);

            //string measureString = "This is a test string will be printed at random offsets!";
            //int    numChars         = measureString.Length;



            ////
            //// Set up the characted ranger array.
            //CharacterRange[] characterRanges = new CharacterRange[numChars];
            //for (int i = 0; i < numChars; i++)
            //    characterRanges[i] = new CharacterRange(i, 1);


            ////
            //// Set up the string format
            //StringFormat stringFormat = new StringFormat();
            //stringFormat.FormatFlags = StringFormatFlags.NoClip;
            //stringFormat.SetMeasurableCharacterRanges(characterRanges);

            //// Set up the array to accept the regions.
            //Region[] stringRegions = new Region[numChars];



            //// The font to use.. 'using' will dispose of it for us
            //using (Font stringFont = new Font("Times New Roman", 16.0F))
            //{

            //    //
            //    // Get the max width.. for the complete length
            //    SizeF size = e.Graphics.MeasureString(measureString, stringFont );

            //    //
            //    // Assume the string is in a stratight line, just to work out the 
            //    // regions. We will adjust the containing rectangles later.
            //    RectangleF layoutRect = 
            //        new RectangleF( 0.0f, 0.0f, size.Width, size.Height);

            //    //
            //    // Caluclate the regions for each character in the string.
            //    stringRegions = e.Graphics.MeasureCharacterRanges(
            //        measureString,
            //        stringFont,
            //        layoutRect,
            //        stringFormat);

            //    //
            //    // Some random offsets, uncomment the DrawRectagle
            //    // if you want to see the bounding box.
            //    Random rand = new Random();
            //    for ( int indx = 0 ; indx < numChars; indx++ )
            //    {
            //        Region region = stringRegions[indx] as Region;
            //        RectangleF rect = region.GetBounds(e.Graphics);
            //        rect.Offset( 0f, (float) rand.Next(100) / 10f);
            //        e.Graphics.DrawString( measureString.Substring(indx,1), 
            //              stringFont, Brushes.Yellow, rect, stringFormat );
            //        e.Graphics.DrawRectangle( Pens.Red, Rectangle.Round( rect ));
            //    }
            //}

            //return (int)(rect.Right + 1.0f);


            ////var form = StringFormat.GenericTypographic;
            //var form = new System.Drawing.StringFormat();

            //e.Graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            //var tt = e.Graphics.MeasureString("Canadian Money Market Fund", f, 100000, form);

            //var regions = new System.Drawing.Region[1];
            //var rect = new System.Drawing.RectangleF(0, 0,1000, 1000);

            //regions = e.Graphics.MeasureCharacterRanges("Canadian Money Market Fund", f, rect, form);
            ////{ GenericTypographic together with FormatFlags.MeasureTrailingSpaces | FormatFlags.NoWrap | FormatFlags.NoClip);



            //var s = regions[0].GetBounds(e.Graphics);            

            //e.Graphics.DrawRectangle(new Pen(Color.Red, 1), 0.0F, 0.0F, 1, 1); // tt2[0].GetBounds(e.Graphics).Width, tt2[0].GetBounds(e.Graphics).Height);

            //// Draw string to screen.
            //e.Graphics.DrawString("Canadian Money Market Fund", f, Brushes.Black, new PointF(0, 0));


        }



        private List<string> GetInstalledFonts()
        {
            var installed_fonts = new InstalledFontCollection();
            var families = installed_fonts.Families;
            var allFonts = new List<string>();


            foreach (FontFamily ff in families)
            {
                allFonts.Add(ff.Name);
            }
            allFonts.Sort();

            return allFonts;
        }

        private void CalcHeight(PaintEventArgs e)
        {
            var graphics = e.Graphics;

            //using (Graphics graphics = Graphics.FromHwnd(IntPtr.Zero))
            //{

                var font = new Font("Times New Roman", 12, FontStyle.Regular); // Use bitwise operator "|" to get Bold and Italics simultaneously                
                graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
                var format = StringFormat.GenericTypographic;

                RectangleF rect = new System.Drawing.RectangleF(0, 0, 1000, 1000); 
                List<RectangleF> rects = new List<RectangleF>();
                Region[] regions = new System.Drawing.Region[1];
                //Need to redo regex to pickup all non-linebreak characters (Eg !,@,#,)
                var matches = Regex.Matches(textBox1.Text, @"\w+");


                float topPadding = 10;
                float y = 0;
                y = topPadding;

                float lineSpacing = 19.2F;    

                foreach (Match match in matches){

                    // Need to resize rect every call to ensure text will fit within it
                    rect = new System.Drawing.RectangleF(0, 0, 1000, 1000);

                    var text = match.ToString();

                    // Range has an internal limit of 32 characters wide
                    CharacterRange[] ranges = { new System.Drawing.CharacterRange(0, text.Length) };                    

                    format.SetMeasurableCharacterRanges(ranges);

                    regions = graphics.MeasureCharacterRanges(text, font, rect, format);
                    rect = regions[0].GetBounds(graphics);
                    rects.Add(rect);

                    // Left Padding
                    var leftPaddingRect = new RectangleF(0, 0, 10, 1000);
                    graphics.DrawRectangle(new Pen(Color.Blue, 1F), 0, 10, leftPaddingRect.Width, leftPaddingRect.Height);

                    // Top Padding
                    var topPaddingRect = new RectangleF(0, 0, 1000, topPadding);
                    graphics.DrawRectangle(new Pen(Color.Yellow, 1F), 0, 0, topPaddingRect.Width, topPaddingRect.Height);
                    
                    float px = 9F;
                    graphics.DrawString(text, font, Brushes.Black, px, y, format);
                    graphics.DrawRectangle(new Pen(Color.Red, 1), px, y, rect.Width, rect.Height);
                    y = y + lineSpacing;
                    
                }

                //SEE ONE NOTE NOTEBOOK ON WHY TOTAL HEIGHT IS CALCULATED THIS WAY
                // Gets draw point of last text-line "box", subtracts line spacing since there's one extra linespacing added at end of for-each loop and then add rect height to get final height.
                // This height is assuming drawing started from y = 0 (which it should always do)
                
                var height = y - lineSpacing + rect.Height;

                //Bottom Padding
                var bottomPaddingRect = new RectangleF(0, 0, 1000, 10);
                graphics.DrawRectangle(new Pen(Color.Orange, 1F), 10, height, bottomPaddingRect.Width, bottomPaddingRect.Height);

        }


        private void DrawList(PaintEventArgs e)
        {
            string drawString = "Sample Text";
            var f = new Font("Times New Roman", 12, FontStyle.Regular);
            System.Drawing.SolidBrush drawBrush = new System.Drawing.SolidBrush(System.Drawing.Color.Black);
            float x = 0.0F;
            float y = 0.0F;
            System.Drawing.StringFormat drawFormat = new System.Drawing.StringFormat();

            for (var i = 0; i <= 30; i++)
            {
                e.Graphics.DrawString(drawString, f, drawBrush, x, y, drawFormat);
                y = y + 24;
            }
        }

        private void BoundText(PaintEventArgs e)
        {
            var graphics = e.Graphics;

            var font = new Font("Times New Roman", 12, FontStyle.Regular); // Use bitwise operator "|" to get Bold and Italics simultaneously
            var text = "Canadian Money Market Fund";

            graphics.TextRenderingHint = TextRenderingHint.AntiAlias;
            var format = StringFormat.GenericTypographic;

            RectangleF rect = new System.Drawing.RectangleF(0, 0, 1000, 1000);
           
            // Range has an internal limit of 32 characters wide
            CharacterRange[] ranges = { new System.Drawing.CharacterRange(0, text.Length) };
            Region[] regions = new System.Drawing.Region[1];

            format.SetMeasurableCharacterRanges(ranges);

            regions = graphics.MeasureCharacterRanges(text, font, rect, format);
            rect = regions[0].GetBounds(graphics);

            graphics.DrawString(text, font, Brushes.Black, 10, 10, format);
            graphics.DrawRectangle(new Pen(Color.Red, 1), 0, 0, rect.Width, rect.Height);

        }
    }
}