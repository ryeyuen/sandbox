﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            var v = new Validator();

            MessageCollector.SubscribeToValidator(v);

            v.Validate();
            v.Validate();
            v.Validate();
            v.Validate();

            var t = MessageCollector.Message;
        }
    }

    public class Validator
    {
        public static string Message;

        public event EventHandler<ValidatorNewErrorEventArgs> NewError;
        public delegate void EventHandler<ValidatorNewErrorEventArgs>(object sender, ValidatorNewErrorEventArgs e);
        public delegate void NewErrorHandler(string stuff, EventArgs e);

        public void Validate()
        {
            var errorEvent = new ValidatorNewErrorEventArgs();
            errorEvent.ErrorMessage = "Validation Failed";

            NewError(this, errorEvent);
        }
    }

    public class ValidatorNewErrorEventArgs
    {
        public string ErrorMessage { get; set; }
    }

    static class MessageCollector
    {
        public static string Message;

        public static void AddMessage(object newMsg, ValidatorNewErrorEventArgs e)
        {
            Message = Message + e.ErrorMessage;
        }

        public static void SubscribeToValidator(Validator v)
        {
            v.NewError += new Validator.EventHandler<ValidatorNewErrorEventArgs>(AddMessage);
        }


    }
}
